#imports and initializations

cardsFileName = 'cards.txt'

#Step 1 -> Initialize our file handling variable.

cardsFile = open(cardsFileName, 'r')

#Step 2 -> Read the contents of the file into a variable as a string.

cards = cardsFile.read()

#Step 3 -> Break giant card string up into a workable array.

cards = cards.split('\n\n')

#Step 4 -> Print out the cards.

for card in cards:
    print card
    raw_input('\nPress Enter to Continue\n')

#Step 5 -> Close the file handler
cardsFile.close

#todo
#Shuffle cards
#create new file if the file is not there already
#add answer checking
#document how cards.txt should be populated
#student suggestions and ideas